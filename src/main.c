/**
 * @file main.c
 * @author Matías S. Ávalos (@tute_avalos)
 * @brief Alterna el LED (PC13) con interrupción del TIM2
 * @version 0.1
 * @date 2020-05-27
 * 
 * Se configura el Timer2 para interrumpir cada 0.5s y alternar el LED de la
 * BluePill (PC13).
 * 
 * El TIM2 contará de 0 a 999 con un prescaler a 36000 sobre el clock de 
 * 72Mhz, y se genera una interrupción cuando se resetea la cuenta (pasa de
 * 999 a 0).
 * 
 * @copyright Copyright (c) 2020
 * 
 * MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

int main(void)
{
    // Se configura el clock del sistema a 72Mhz
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // Se configura el LED de la BluePill (PC13) como Salida Push-Pull
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

    // Se habilita el clock del periférico del TIM2
    rcc_periph_clock_enable(RCC_TIM2);

    // Se configura el modo de funcionamiento del Timer2
    timer_set_mode(
        TIM2,               // Timer2
        TIM_CR1_CKD_CK_INT, // fuente Clk interno
        TIM_CR1_CMS_EDGE,   // Alineado por flanco
        TIM_CR1_DIR_UP);    // Cuenta ascendente

    timer_set_prescaler(TIM2, 35999); // 72MHz / 36000 => 2KHz

    // Se setea el valor hasta donde se cuenta:
    timer_set_period(TIM2, 999); // 2KHz / 1000 = 2Hz => T = 0.5s

    // Se habilita al interrupción por overflow
    timer_enable_irq(TIM2, TIM_DIER_UIE);

    // Empieza a contar el Timer2
    timer_enable_counter(TIM2);

    // Se habilita la interrupción desde el NVIC
    nvic_enable_irq(NVIC_TIM2_IRQ);

    while (1)
        ; // Nada por hacer, esperando la interrupción
}

void tim2_isr(void)
{                                       // Cada 0.5s:
    timer_clear_flag(TIM2, TIM_SR_UIF); // Se baja el flag de la interrupción
    gpio_toggle(GPIOC, GPIO13);         // Se alterna el LED
}
