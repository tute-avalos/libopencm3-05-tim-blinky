# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 05: Timer Blinky

Este ejemplo está pensado para ver los aspectos básicos de ls TIMs en el STM32F1xx configurandolo para invertir el LED en PC13 del la BluePill en cada interrupción (cada 0.5s).

Entrada del blog: [Programando los TIMs (Timers) del STM32F1 con libOpenCM3 [Parte 1]](https://electronlinux.wordpress.com/2020/07/03/programando-los-tims-timers-del-stm32f1-con-libopencm3-parte-1/)

Video en YouTube: 

